import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import App from './App';

Enzyme.configure({ adapter: new EnzymeAdapter() })

const setup = (props) => {
  return shallow(<App />)
}

test('App renders without error', () => {
  const wrapper = setup()
});
