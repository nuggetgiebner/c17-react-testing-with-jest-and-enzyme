import { actionTypes } from '../Actions'
import successReducer from './successReducer'

test('returns default initial state FALSE when no action is passed', () => {
    const newState = successReducer(undefined, {})
    expect(newState).toBe(false)
})

test('returns state of TRUE upon receiving an action type: CORRECT_GUESS', () => {
    const newState = successReducer(undefined, { type: actionTypes.CORRECT_GUESS })
    expect(newState).toBe(true)
})