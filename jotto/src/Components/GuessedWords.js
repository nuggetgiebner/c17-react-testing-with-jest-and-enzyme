import React from 'react'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'

const GuessedWords = (props) => {

    const { guessedWords } = props

    const listOfWords = guessedWords.map( (word, i) => {
        return (<Grid container key={i} data-test='GuessedWords-word'>
            <Grid container justify='center' item xs={6} style={{border: '2px solid black', borderRight: '0px solid black', borderTop: '0px solid black', padding: '0.5rem'}}>
                <Typography variant='h5'>
                    {word.guessedWord}
                </Typography>
            </Grid>
            <Grid container justify='center' item xs={6} style={{border: '2px solid black', borderTop: '0px solid black', padding: '0.5rem'}}>
                <Typography variant='h5'>
                    {word.letterMatchCount}
                </Typography>
            </Grid>
        </Grid>)
    })

    return (
        <Box data-test='GuessedWords-component'>
            <Container>
                <Grid container direction='row' justify='center'>
                    {guessedWords.length === 0 ?
                        <Grid item xs={12}>
                            <Typography variant='h6' data-test='GuessedWords-instructions'>
                                Try to guess the secret word!
                            </Typography>
                        </Grid>
                        :
                        <Grid container direction='row' data-test='GuessedWords-word-table'>
                            <Grid container justify='center' item xs={12} style={{border: '2px solid black', borderBottom: '0px solid black', padding: '0.5rem', backgroundColor: 'rgba(211,211,211,0.85)'}}>
                                <Typography variant='h5' >
                                    Guessed Words
                                </Typography>
                            </Grid>
                            <Grid container justify='center' item xs={6} style={{border: '2px solid black', borderRight: '0px solid black', padding: '0.5rem', backgroundColor: 'rgba(211,211,211,0.4)'}}>
                                <Typography variant='h5'>
                                    Guess
                                </Typography>
                            </Grid>
                            <Grid container justify='center' item xs={6} style={{border: '2px solid black', padding: '0.5rem', backgroundColor: 'rgba(211,211,211,0.4)'}}>
                                <Typography variant='h5'>
                                    Matching Letters
                                </Typography>
                            </Grid>
                            {listOfWords}
                        </Grid>}
                </Grid>
            </Container>
        </Box>
    )
}

GuessedWords.propTypes = {
    guessedWords: PropTypes.arrayOf(
        PropTypes.shape({
            guessedWord: PropTypes.string.isRequired,
            letterMatchCount: PropTypes.number.isRequired
        })
    ).isRequired
}

export default GuessedWords
