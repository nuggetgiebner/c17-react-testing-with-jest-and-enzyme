import React from 'react'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'

const Congrats = (props) => {

    const { success } = props

    return (
        <Box>
            <Container>
                <Grid container>
                    {success ?
                        <Typography data-test='congrats-success' variant='h5'>Congratulations! You guesses the word!</Typography> :
                        <Typography data-test='congrats-no-success' variant='h5'></Typography>
                    }
                </Grid>
            </Container>
        </Box>
    )
}

Congrats.propTypes = {
    success: PropTypes.bool.isRequired
}

export default Congrats
