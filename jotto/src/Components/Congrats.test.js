import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import Congrats from './Congrats';
import { checkProps, findByTestAttr } from '../../test/testUtils.js'


Enzyme.configure({ adapter: new EnzymeAdapter() })

const defaultProps = { success: false }

const setup = (props={}) => {
    const setupProps = { ...defaultProps, ...props }
    return shallow(<Congrats {...setupProps} />)
}

test('Congrats component renders without error', () => {
    const wrapper = setup({ success: false })
})

test('renders no text when success prop is false', () => {
    const wrapper = setup({ success: false })
    const noSuccessText = findByTestAttr(wrapper, 'congrats-no-success')
    expect(noSuccessText.text()).toBe('')
})

test('renders non-empty congrats message when success prop is true', () => {
    const wrapper = setup({ success: true })
    const successText = findByTestAttr(wrapper, 'congrats-success')
    expect(successText.text().length).not.toBe(0)
})

test('does not throw warning with expected props', () => {
    const expectedProps = { success: false }
    checkProps(Congrats, expectedProps)
})