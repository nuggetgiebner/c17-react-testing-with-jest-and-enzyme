import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import GuessedWords from './GuessedWords';
import { checkProps, findByTestAttr } from '../../test/testUtils.js'

const defaultProps = {
    guessedWords: [{ guessedWord: 'train', letterMatchCount: 3 }]
}

Enzyme.configure({ adapter: new EnzymeAdapter() })

/**
 * Factory function to create a ShallowWrapper for the GuessedWords component
 * @function setup
 * @param { object } props - Component props specific to this setup
 * @ returns { ShallowWrapper }
 */
const setup = (props={}) => {
    const setupProps = { ...defaultProps, ...props }
    return shallow(<GuessedWords {...setupProps} />)
}

test('GuessedWords renders without error', () => {
    const wrapper = setup({ guessedWords: [] })
})

test('does not throw warning with expected props', () => {
    checkProps(GuessedWords, defaultProps)
})

describe('if there are no words guessed', () => {
    let wrapper
    beforeEach( () => {
        wrapper = setup({ guessedWords: [] })
    } )

    test('renders without error', () => {
        const component = findByTestAttr(wrapper, 'GuessedWords-component')
        expect(component.length).toBe(1)
    })

    test('renders instructions to guess a word', () => {
        const instructions = findByTestAttr(wrapper, 'GuessedWords-instructions')
        expect(instructions.text().length).not.toBe(0)
    })
})

describe('if there are words guessed', () => {
    let wrapper
    let guessedWords = [{ guessedWord: 'train', letterMatchCount: 0 },
                       { guessedWord: 'cabin', letterMatchCount: 2 },
                       { guessedWord: 'party', letterMatchCount: 5 }]
    beforeEach( () => {
        wrapper = setup({ guessedWords })
    } )

    test('renders without error', () => {
        const component = findByTestAttr(wrapper, 'GuessedWords-component')
        expect(component.length).toBe(1)
    })

    test('renders word table', () => {
        const wordTable = findByTestAttr(wrapper, 'GuessedWords-word-table')
        expect(wordTable.text().length).not.toBe(0)
    })

    test('displays correct number of guessed words', () => {
        const wordTable = findByTestAttr(wrapper, 'GuessedWords-word')
        expect(wordTable.length).toBe(guessedWords.length)
    })
})