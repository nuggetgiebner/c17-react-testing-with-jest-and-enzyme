import React, { Component } from 'react';
import './App.css';
import Box from '@material-ui/core/Box'
import Congrats from './Components/Congrats'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import GuessedWords from './Components/GuessedWords'
import Typography from '@material-ui/core/Typography'

class App extends Component {
  // {guessedWord: "", letterMatchCount: 0}
  constructor(props) {
    super(props)
    this.state = {
      guessedWords: [{ guessedWord: 'train', letterMatchCount: 0 },
      { guessedWord: 'cabin', letterMatchCount: 2 },
      { guessedWord: 'party', letterMatchCount: 5 }],
      success: false
    }
  }

  render() {

    const { guessedWords, success } = this.state

    return (
      <Box>
        <Container>
          <Grid container>
            <Grid container justify='center' item xs={12} style={{margin: '2rem'}}>
              <Typography variant='h4'>
                Jotto
              </Typography>
            </Grid>
            <Congrats success={success} />
            <GuessedWords guessedWords={guessedWords} />
          </Grid>
        </Container>
      </Box>
    );
  }
}

export default App;
