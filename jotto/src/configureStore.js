import { createStore } from 'redux'
import rootReducer from './Redux/Reducers'

export default createStore(rootReducer)