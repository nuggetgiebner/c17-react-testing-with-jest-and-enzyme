import React, { Component } from 'react';
import './App.css';
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      counter: 0,
      positiveCounter: true
    }
  }

  handleClick = () => {
    const { counter } = this.state
    if(counter === 0){this.setState({positiveCounter: true})}
    this.setState({ counter: this.state.counter + 1 })
  }

  handleDecrementClick = () => {
    const { counter } = this.state
    if(counter === 0){this.setState({
      positiveCounter: false
    })}
    else {this.setState({counter: counter - 1})}
  }

  render() {

    const { counter, positiveCounter } = this.state

    return (
      <Box data-test='component-app'>
        <Container>
          <Grid container direction='row' style={{ marginTop: '5rem' }}>
            <Grid item xs={12} style={{ marginTop: '1rem' }}>
              { positiveCounter ?
                <Typography variant='h6' align='center' data-test='counter-display'>
                  The counter is currently: {counter}
                </Typography> :
                <Typography variant='h6' align='center' data-test='counter-display'>
                  Count Below Zero Is Not Allowed
                </Typography>
              }
            </Grid>
            <Grid item xs={12} align='center' style={{ marginTop: '1rem' }}>
              <button data-test='increment-button' onClick={this.handleClick}>Increment Counter</button>
            </Grid>
            <Grid item xs={12} align='center' style={{ marginTop: '1rem' }}>
              <button data-test='decrement-button' onClick={this.handleDecrementClick}>Decrement Counter</button>
            </Grid>
          </Grid>
        </Container>
      </Box>
    );
  }
}

export default App;
