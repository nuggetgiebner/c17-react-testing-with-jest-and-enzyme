import React from 'react';
import Enzyme, { shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import App from './App';

Enzyme.configure({ adapter: new EnzymeAdapter() })

/******************************************************************************
* Factory function to create a ShallowWrapper for the App component
* @function setup
* @param {object} props - Component props specific to this setup
* @param {object} state - Initial state for setup
* @returns {ShallowWrapper} */

const setup = (props={}, state=null) => {
  const wrapper = shallow(<App {...props} />)
  if(state) wrapper.setState(state)
  return wrapper
}
/*****************************************************************************/

/******************************************************************************
* Factory function to create a ShallowWrapper for the App component
* @returns {ShallowWrapper} containing node(s) with the given data-test text value
* @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within
* @param {string} text - text of data-test attribute for search
* @returs {ShallowWrapper} */

const findByTestAttr = (wrapper, text) => {
  return wrapper.find(`[data-test='${text}']`)
}
/*****************************************************************************/

test('renders without error', () => {
  const wrapper = setup()
  const appComponent = findByTestAttr(wrapper, 'component-app')
  expect(appComponent.length).toBe(1)
})

test('renders increment button', () => {
  const wrapper = setup()
  const button = findByTestAttr(wrapper, 'increment-button')
  expect(button.length).toBe(1)
})

test('renders counter display', () => {
  const wrapper = setup()
  const counterDisplay = findByTestAttr(wrapper, 'counter-display')
  expect(counterDisplay.length).toBe(1)
})

test('counter starts at zero', () => {
  const wrapper = setup()
  const initialCounterState = wrapper.state('counter')
  expect(initialCounterState).toBe(0)
})

test('clicking button increments counter display', () => {
  const counter = 7
  const wrapper = setup(null, { counter })
  const button = findByTestAttr(wrapper, 'increment-button')
  button.simulate('click')
  const counterDisplay = findByTestAttr(wrapper, 'counter-display')
  expect(counterDisplay.text()).toContain(counter + 1)
})

test('renders decrement button', () => {
  const wrapper = setup()
  const decrementButton = findByTestAttr(wrapper, 'decrement-button')
  expect(decrementButton.length).toBe(1)
})

test('clicking decrement button decrements counter display', () => {
  const counter = 7
  const wrapper = setup(null, { counter })
  const decrementButton = findByTestAttr(wrapper, 'decrement-button')
  decrementButton.simulate('click')
  const display = findByTestAttr(wrapper, 'counter-display')
  expect(display.text()).toContain(counter - 1)
})

test('dont decrement the counter', () => {
  const wrapper = setup(null, {counter: 0})
  const decrementButton = findByTestAttr(wrapper, 'decrement-button')
  decrementButton.simulate('click')
  const counter = wrapper.state('counter')
  expect(counter).toBe(0)
})

test('display error message', () => {
  const wrapper = setup(null, {counter: 0})
  const decrementButton = findByTestAttr(wrapper, 'decrement-button')
  decrementButton.simulate('click')
  const display = findByTestAttr(wrapper, 'counter-display')
  expect(display.text()).toContain('Count Below Zero Is Not Allowed')
})

test('clear error on increment', () => {
  const wrapper = setup(null, { counter: 0 })
  const decrementButton = findByTestAttr(wrapper, 'decrement-button')
  decrementButton.simulate('click')
  const incrementButton = findByTestAttr(wrapper, 'increment-button')
  incrementButton.simulate('click')
  const display = findByTestAttr(wrapper, 'counter-display')
  expect(display.text()).toContain('The counter is currently:')
  expect(display.text()).toContain(1)
})